﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{

    static class CONST 
    {
        public const string RCVPATH = "C:\\TLG\\RCV\\";
        public const string SNDPATH = "C:\\TLG\\SND\\";
        public const string ARCPATH = "C:\\TLG\\ARC\\";
        public const string TMPLT   = "C:\\TLG\\Templates\\";
        public const string OURADDR = "УКЛЖЫТЫЕ";
        public const string RCVADDR = "УКККЗДЗЬ УККРДКДУ УКЛЖЗЯЗЬ УККРЫТЫД УККРДТЯТ УКЛЖЫТЫЕ УКККЫЛЫЬ";
        public const int PATHLENGTH = 11;
        public const int ENCODING = 1251;
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int msgIndex = 0;
        public string DATETIME = "000000";
        //public ArrayList readStatus = new ArrayList();

        public MainWindow()
        {
            InitializeComponent();
            //this.Icon = new System.Drawing.Icon(Properties.Resources.bad);
            //this.Icon = Properties.Resources.bad;

            //Check all working folders
            ArrayList missingFolders = MyFileProcessor.WorkingFoldersExist();
            if(missingFolders.Count > 0)
            {
                string msg = "";
                foreach(string item in missingFolders)
                {
                    msg += "Папка: " + item + " не існує" + "\n";
                }
                MessageBox.Show(msg);
                Application.Current.Shutdown();
                return;
            }

            msgIndex = MyFileProcessor.GetLastMSGIndex();

            RecieversAddr.Text = CONST.RCVADDR;
            SenderAddr.Text = CONST.OURADDR;
            UpdateInboxFileList();
            UpdateOutboxFileList();

            ////DateTime Thread
            BackgroundWorker timeThread = new BackgroundWorker();
            timeThread.DoWork += new DoWorkEventHandler(thrdWork);
            timeThread.RunWorkerAsync();
        }
        //DateTime Thread Work Function
        //Updates DATETIME variable, Updates InboxFileList
        //Show MessageBox if there are telegrams in folder \SND
        public void thrdWork(object sender, DoWorkEventArgs e)
        {
            int counter = 0;
            while(true)
            {
                counter++;
                DATETIME = DateTime.UtcNow.ToString("ddHHmm");
                Dispatcher.Invoke((Action)(() => DateTimeBox.Text = DATETIME));
                if (counter == 4)
                {
                    Dispatcher.Invoke((Action)(() => UpdateInboxFileList()));
                    counter = 0;
                    if (Directory.GetFiles(CONST.SNDPATH).Any())
                        MessageBox.Show("Є не відправлені телеграми! Перевірте роботу драйвера AFTN, або працездатність каналу №13");
                }
                
                Thread.Sleep(10000);
            }
        }
        ////Get file content in inboxListView
        private void inboxListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                inboxPreviewBox.Text = AFTNMessage.DecodeMSG(e, CONST.RCVPATH);
                // For read status
                //string fileName = e.AddedItems[0].ToString();
                //if(!readStatus.Contains(fileName))
                //    readStatus.Add(fileName);
            }
        }
        ////Get file content in sendListView
        private void sendListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                sendPreviewBox.Text = AFTNMessage.DecodeMSG(e, CONST.ARCPATH);
                // For read status
                //string fileName = e.AddedItems[0].ToString();
                //if(!readStatus.Contains(fileName))
                //    readStatus.Add(fileName);
            }
        }
        private void SendBTN_Click(object sender, RoutedEventArgs e)
        {
            string importance = importanceBox.Text;
            if (importance == "СС")
            {
                MessageBox.Show("Вибрано терміновість \"СС\"!!!\n Уважно перевірте телеграму!!!");
            }
            string rcvaddr = RecieversAddr.Text;
            if (rcvaddr.Length > 62)
            {
                MessageBox.Show("Перевищено кількість адрес у полі отримувачів!");
                return;
            }
            string msg = OutMessageBox.Text;
            ++msgIndex;
            string fileName = DATETIME + msgIndex.ToString("D4");

            //
            if (msg.Length < 4)
            {
                MessageBox.Show("Текст повідомлення занадто короткий!");
                return;
            }
            string formatedMSG = AFTNMessage.FormatMSG(importance, rcvaddr, DATETIME, msg);
            MessageBoxResult msgResult = MessageBox.Show("Відправити повідомлення?", "", MessageBoxButton.YesNo);
            if (msgResult == MessageBoxResult.Yes)
            {
                File.WriteAllText(CONST.ARCPATH + fileName, formatedMSG, Encoding.GetEncoding(CONST.ENCODING));
                File.Copy(CONST.ARCPATH + fileName, CONST.SNDPATH + fileName);
                UpdateOutboxFileList();
                OutMessageBox.Clear();
            }
        }
        private void UpdateBTN_Click(object sender, RoutedEventArgs e)
        {
            UpdateInboxFileList();
        }
        private void TemplatesBTN_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = CONST.TMPLT;
            if (openFileDialog.ShowDialog() == true)
                OutMessageBox.Text = File.ReadAllText(openFileDialog.FileName, Encoding.GetEncoding(CONST.ENCODING));
        }
        //fill in inbox
        public void UpdateInboxFileList()
        {
            string[] rcvlist = MyFileProcessor.GetFileList(CONST.RCVPATH);
            inboxListView.Items.Clear();
            for (int i = 0; i < rcvlist.Length; i++)
            {
                inboxListView.Items.Insert(i, rcvlist[i].Substring(CONST.PATHLENGTH));
            }
        }
        //fill in outbox
        public void UpdateOutboxFileList()
        { 
            string[] outlist = MyFileProcessor.GetFileList(CONST.ARCPATH);
            sendListView.Items.Clear();
            for (int i = 0; i < outlist.Length; i++)
            {
                sendListView.Items.Insert(i, outlist[i].Substring(CONST.PATHLENGTH));
            }
        }
        private void Print_Click(object sender, RoutedEventArgs e)
        {
            AFTNMessage.TemplatePrint(importanceBox.Text, RecieversAddr.Text, SenderAddr.Text, OutMessageBox.Text);
        }
        //protected override void OnClosing(CancelEventArgs e)
        //{
        //    using (StreamWriter sw = new StreamWriter("DB"))
        //    {
        //        foreach (string str in readStatus)
        //            sw.WriteLine(str);
        //        sw.Close();
        //    }
        //    base.OnClosing(e);
        //}
    }

    public static class MyFileProcessor
    {
        public static ArrayList WorkingFoldersExist ()
        {
            ArrayList missingFolders = new ArrayList();
            if (!Directory.Exists(CONST.RCVPATH))
                missingFolders.Add(CONST.RCVPATH);
            if (!Directory.Exists(CONST.ARCPATH))
                missingFolders.Add(CONST.ARCPATH);
            if (!Directory.Exists(CONST.SNDPATH))
                missingFolders.Add(CONST.SNDPATH);
            if (!Directory.Exists(CONST.TMPLT))
                missingFolders.Add(CONST.TMPLT);
            return missingFolders;
        }
        public static string[] GetFileList(string path)
        {
            string[] filelist;
                if (Directory.Exists(path))
                {
                    // This path is a directory
                    filelist = ProcessDirectory(path);
                }
                else
                {
                filelist = new string[10];
                for (int i = 0; i<filelist.Length; i++)
                    filelist[i] = path + " DOESN\'T EXIST";
                }
            return filelist;
        }
        public static int GetLastMSGIndex()
        {
            int index = 0;
            DirectoryInfo directory = new DirectoryInfo(CONST.ARCPATH);
            FileInfo[] filelist = directory.GetFiles();
            if (filelist.Count() > 0)
            {
                string stringIndex = filelist.OrderByDescending(f => f.LastWriteTime).First().Name.Substring(6);
                Int32.TryParse(stringIndex, out index);
            }
            return index;
        }
        public static string[] ProcessDirectory(string targetDirectory)
        {
            return Directory.GetFiles(targetDirectory);
        }
    }

    public static class AFTNMessage
    {
        public static string FormatMSG(string importance, string rcvaddr, string dt, string msg)
        {
            return String.Format("{0} {1}\r\r\n{2} {3}\r\n{4}\r\n\n\n\n\n\n\n\n", importance, rcvaddr, dt, CONST.OURADDR, msg);
        }
        public static string DecodeMSG(SelectionChangedEventArgs selected, string path)
        {
            string formated = "";
            string line;
            using (StreamReader srfileStream = new StreamReader((Stream)File.OpenRead(path + selected.AddedItems[0].ToString()), Encoding.GetEncoding(CONST.ENCODING)))
            {
                while (srfileStream.Peek() >= 0)
                {
                    line = srfileStream.ReadLine();
                    if (!string.IsNullOrEmpty(line))
                        formated += line + "\n";
                }
            }
            return formated.ToString();
        }
        public static void TemplatePrint(string importance, string rcvaddr, string sndaddr, string msg)
        {
            FlowDocument flowDOC = new FlowDocument();
            flowDOC.PageHeight = 29.7 * (96 / 2.54);            //1122
            flowDOC.PageWidth = 21 * (96 / 2.54);               //793
            flowDOC.PagePadding = new Thickness(95, 40, 40, 40);// W658 H1042
            flowDOC.FontFamily = new System.Windows.Media.FontFamily("Arial");
            flowDOC.FontSize = 14.0;
            flowDOC.ColumnWidth = 658.0;

            // Header
            Paragraph paragraph = new Paragraph(new Run("ТЕЛЕГРАМА"));
            paragraph.TextAlignment = TextAlignment.Center;
            paragraph.FontSize = 18.0;
            paragraph.FontWeight = FontWeights.Bold;
            flowDOC.Blocks.Add(paragraph);

            // Trying GRID!!!!
            Grid myGrid = new Grid();
            myGrid.Width = 658.0;
            myGrid.HorizontalAlignment = HorizontalAlignment.Left;
            myGrid.VerticalAlignment = VerticalAlignment.Center;

            // Define the Columns
            ColumnDefinition colDef1 = new ColumnDefinition();
            ColumnDefinition colDef2 = new ColumnDefinition();
            ColumnDefinition colDef3 = new ColumnDefinition();
            ColumnDefinition colDef4 = new ColumnDefinition();
            ColumnDefinition colDef5 = new ColumnDefinition();
            colDef1.Width = new GridLength(30.0);
            colDef2.MaxWidth = 30.0;
            myGrid.ColumnDefinitions.Add(colDef1);
            myGrid.ColumnDefinitions.Add(colDef2);
            myGrid.ColumnDefinitions.Add(colDef3);
            myGrid.ColumnDefinitions.Add(colDef4);
            myGrid.ColumnDefinitions.Add(colDef5);


            // Define the Rows
            RowDefinition rowDef1 = new RowDefinition();
            RowDefinition rowDef2 = new RowDefinition();
            RowDefinition rowDef3 = new RowDefinition();
            RowDefinition rowDef4 = new RowDefinition();
            RowDefinition rowDef5 = new RowDefinition();
            RowDefinition rowDef6 = new RowDefinition();
            rowDef1.MinHeight = 25.0;
            rowDef2.MinHeight = 25.0;
            rowDef3.MinHeight = 700.0;
            rowDef4.MinHeight = 80.0;
            rowDef5.MinHeight = 60.0;
            rowDef6.MinHeight = 80.0;
            myGrid.RowDefinitions.Add(rowDef1);
            myGrid.RowDefinitions.Add(rowDef2);
            myGrid.RowDefinitions.Add(rowDef3);
            myGrid.RowDefinitions.Add(rowDef4);
            myGrid.RowDefinitions.Add(rowDef5);
            myGrid.RowDefinitions.Add(rowDef6);

            // Add the first text cell to the Grid
            TextBlock importanceTXT = new TextBlock();
            importanceTXT.Inlines.Add(new Run(importance + "\n") { FontWeight = FontWeights.Bold, FontSize = 14 });
            importanceTXT.TextAlignment = TextAlignment.Center;
            importanceTXT.VerticalAlignment = VerticalAlignment.Center;
            Grid.SetColumn(importanceTXT, 0);
            Grid.SetColumnSpan(importanceTXT, 2);
            Grid.SetRow(importanceTXT, 0);

            // Add the second text cell to the Grid
            TextBlock recievirsTXT = new TextBlock();
            recievirsTXT.Inlines.Add(new Run(rcvaddr) { FontWeight = FontWeights.Bold, FontSize = 14 });
            recievirsTXT.Inlines.Add(new Run("\n\t\t\t\t\t\t\t\t\t\t") { TextDecorations = TextDecorations.Underline, FontWeight = FontWeights.Normal, FontSize = 4 });
            recievirsTXT.VerticalAlignment = VerticalAlignment.Center;
            recievirsTXT.Padding = new Thickness(5.0, 0.0, 5.0, 0.0);
            Grid.SetRow(recievirsTXT, 0);
            Grid.SetColumn(recievirsTXT, 2);
            Grid.SetColumnSpan(recievirsTXT, 4);

            // Add the third text cell to the Grid
            //TextBlock datetimeTXT = new TextBlock();
            //datetimeTXT.Text = DateTime.UtcNow.ToString("dd");
            //datetimeTXT.FontSize = 14;
            //datetimeTXT.FontWeight = FontWeights.Bold;
            //datetimeTXT.TextAlignment = TextAlignment.Center;
            //datetimeTXT.VerticalAlignment = VerticalAlignment.Center;
            //Grid.SetRow(datetimeTXT, 1);
            //Grid.SetColumn(datetimeTXT, 0);
            //Grid.SetColumnSpan(datetimeTXT, 2);

            // Add the fourth text cell to the Grid
            TextBlock senderTXT = new TextBlock();
            senderTXT.Inlines.Add(new Run(sndaddr) { FontWeight = FontWeights.Bold, FontSize = 14 } );
            senderTXT.VerticalAlignment = VerticalAlignment.Center;
            senderTXT.Padding = new Thickness(5.0, 0.0, 5.0, 0.0);
            Grid.SetRow(senderTXT, 1);
            Grid.SetColumn(senderTXT, 2);
            Grid.SetColumnSpan(senderTXT, 3);

            // Add the fifth text cell to the Grid
            TextBlock msgTXT = new TextBlock();
            msgTXT.FontWeight = FontWeights.Normal;
            msgTXT.Text = msg;
            msgTXT.FontSize = 14;
            msgTXT.Padding = new Thickness(5.0, 10.0, 5.0, 0.0);
            Grid.SetRow(msgTXT, 2);
            Grid.SetColumn(msgTXT, 0);
            Grid.SetColumnSpan(msgTXT, 5);

            // Add the seventh text cell to the Grid
            TextBlock txt6 = new TextBlock();
            txt6.LineHeight = 20.0;
            txt6.Inlines.Add(new Run("Дата: " + DateTime.UtcNow.ToString("dd.MM.yyyy") + "\n") { FontWeight = FontWeights.Bold });
            txt6.Inlines.Add(new Run("Передачу телеграми дозволяю:\n") { FontWeight = FontWeights.Bold });
            txt6.Inlines.Add(new Run("Провідний інженер з РН та РЛ\t\t") { FontWeight = FontWeights.Normal });
            txt6.Inlines.Add(new Run("\t\t\t") { TextDecorations = TextDecorations.Underline, FontWeight = FontWeights.Normal });
            txt6.Inlines.Add("    ");
            txt6.Inlines.Add(new Run("\t\t\t\t\n") { TextDecorations = TextDecorations.Underline, FontWeight = FontWeights.Normal });
            txt6.Inlines.Add(new Run("\t\t\t\t\t\t  (підпис)\t\t\t(прізвище, ім'я, по батькові)") { FontWeight = FontWeights.Normal, FontSize = 8 });

            Grid.SetRow(txt6, 3);
            Grid.SetColumn(txt6, 0);
            Grid.SetColumnSpan(txt6, 5);

            // Add the seventh text cell to the Grid
            TextBlock txt7 = new TextBlock();
            txt7.LineHeight = 20.0;
            txt7.Inlines.Add(new Run("Погоджено:\n") { FontWeight = FontWeights.Bold });
            txt7.Inlines.Add(new Run("Керівник польотів РДЦ(АДВ)\t\t") { FontWeight = FontWeights.Normal });
            txt7.Inlines.Add(new Run("\t\t\t") { TextDecorations = TextDecorations.Underline, FontWeight = FontWeights.Normal });
            txt7.Inlines.Add("    ");
            txt7.Inlines.Add(new Run("\t\t\t\t\n") { TextDecorations = TextDecorations.Underline, FontWeight = FontWeights.Normal });
            txt7.Inlines.Add(new Run("\t\t\t\t\t\t  (підпис)\t\t\t(прізвище, ім'я, по батькові)") { FontWeight = FontWeights.Normal, FontSize = 8 });

            Grid.SetRow(txt7, 4);
            Grid.SetColumn(txt7, 0);
            Grid.SetColumnSpan(txt7, 5);

            // Add the seventh text cell to the Grid
            TextBlock txt8 = new TextBlock();
            txt8.LineHeight = 25.0;
            txt8.FontWeight = FontWeights.Normal;
            txt8.TextAlignment = TextAlignment.Justify;
            txt8.Inlines.Add("\nПередачу телеграми погоджено по телефону з заступником директора із ЗНС Львівського\nРСП Украероруху в ");
            txt8.Inlines.Add(new Run("\t\t") { TextDecorations = TextDecorations.Underline });
            txt8.Inlines.Add(" год ");
            txt8.Inlines.Add(new Run("\t\t") { TextDecorations = TextDecorations.Underline });
            txt8.Inlines.Add(" хв.(UTC).");

            Grid.SetRow(txt8, 5);
            Grid.SetColumn(txt8, 0);
            Grid.SetColumnSpan(txt8, 5);

            // Add the TextBlock elements to the Grid Children collection
            myGrid.Children.Add(importanceTXT);
            myGrid.Children.Add(recievirsTXT);
            //myGrid.Children.Add(datetimeTXT);
            myGrid.Children.Add(senderTXT);
            myGrid.Children.Add(msgTXT);
            myGrid.Children.Add(txt6);
            myGrid.Children.Add(txt7);
            myGrid.Children.Add(txt8);

            BlockUIContainer block = new BlockUIContainer(myGrid);

            Table table = new Table();
            TableRowGroup rg = new TableRowGroup();
            TableCell cell = new TableCell();
            cell.Blocks.Add(block);
            TableRow row = new TableRow();
            row.Cells.Add(cell);
            rg.Rows.Add(row);
            table.RowGroups.Add(rg);
            flowDOC.Blocks.Add(table);

            try
            {
                IDocumentPaginatorSource idpSource = flowDOC;
                PrintDialog pd = new PrintDialog();
                //pd.PrintDocument(idpSource.DocumentPaginator, "test print");
                Nullable<Boolean> print = pd.ShowDialog();
                if (print == true)
                {
                    pd.PrintDocument(idpSource.DocumentPaginator, "test print");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка при друці\n" + ex.Message);
            }
        }
    }
}
